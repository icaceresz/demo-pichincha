
# Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
# Click nbfs://nbhost/SystemFileSystem/Templates/Other/Dockerfile to edit this template

FROM openjdk:8-jdk-alpine
RUN addgroup -S pichincha && adduser -S pichincha -G pichincha
USER pichincha:pichincha
ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} demo-pichincha-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java","-jar","/demo-pichincha-0.0.1-SNAPSHOT.jar"]
