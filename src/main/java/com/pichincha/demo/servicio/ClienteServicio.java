/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.pichincha.demo.servicio;

import com.pichincha.demo.entidad.Cliente;
import com.pichincha.demo.reporitory.ClienteRepository;
import com.pichincha.demo.util.ApiException;
import com.pichincha.demo.util.Mensajes;
import java.util.List;
import java.util.Objects;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author icaceres
 */
@Service
public class ClienteServicio {

    @Autowired
    private ClienteRepository clienteRepository;

    @Transactional
    public Cliente guardar(Cliente cliente) throws ApiException {
        try {
            return clienteRepository.save(cliente);
        } catch (RuntimeException e) {
            throw new ApiException(e);
        }
    }

    @Transactional
    public Cliente actualizar(Cliente cliente, Long id) throws ApiException {
        try {
            Cliente entidad = clienteRepository.getById(id);
            if (Objects.nonNull(entidad)) {
                cliente.setId(id);
                cliente.getPersona().setId(entidad.getPersona().getId());
            }
            clienteRepository.save(cliente);
            return cliente;
        } catch (RuntimeException e) {
            throw new ApiException(e);
        }
    }

    @Transactional
    public void eliminar(Long id) throws ApiException {
        Cliente cliente = clienteRepository.getById(id);
        if (Objects.isNull(cliente)) {
            throw new ApiException(Mensajes.REGISTRO_NO_EXISTE);
        } else {
            cliente.setEstado(Boolean.FALSE);
            clienteRepository.save(cliente);
        }
    }

    public List<Cliente> listarActivos() {
        return clienteRepository.listarActivos();
    }

}
