/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.pichincha.demo.servicio;

import com.pichincha.demo.entidad.Catalogo;
import com.pichincha.demo.entidad.Cliente;
import com.pichincha.demo.entidad.Cuenta;
import com.pichincha.demo.entidad.Movimiento;
import com.pichincha.demo.reporitory.CatalogoRepository;
import com.pichincha.demo.reporitory.ClienteRepository;
import com.pichincha.demo.reporitory.CuentaRepository;
import com.pichincha.demo.reporitory.MovimientoRepository;
import com.pichincha.demo.to.MovimientoTo;
import com.pichincha.demo.util.ApiException;
import com.pichincha.demo.util.CatalogoEnum;
import com.pichincha.demo.util.FechaUtil;
import com.pichincha.demo.util.Mensajes;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author icaceres
 */
@Service
public class MovimientoServicio {

    @Autowired
    private MovimientoRepository movimientoRepository;

    @Autowired
    private CatalogoRepository catalogoRepository;

    @Autowired
    private ClienteRepository clienteRepository;

    @Autowired
    private CuentaRepository cuentaRepository;

    @Value("${com.pichincha.limite.diario}")
    private String limiteDiario;

    @Transactional
    public void guardar(Movimiento movimiento) throws ApiException {
        try {
            Cliente cliente = clienteRepository.obtenerPorIdentificacion(movimiento.getIdentificacionCliente());
            Cuenta cuenta = cuentaRepository.findByCliente(cliente);

            Catalogo tipoMovimiento = catalogoRepository.findByCodigo(movimiento.getTipo().getCodigo());
            Catalogo movimientoRetiro = catalogoRepository.findByCodigo(CatalogoEnum.MOVIMIENTO_RETIRO.getCodigo());
            Movimiento ultimoMovimiento = movimientoRepository.obtenerUltimoPorCuenta(cuenta.getId());
            List<Movimiento> listaMovimientoRetiro = movimientoRepository.listarPorFechaTipoMovimiento(cuenta, FechaUtil.obtenerFechaDesde(new Date()), FechaUtil.obtenerFechaHasta(new Date()), movimientoRetiro);
            //Parametro limiteRetiro = parametroServicio.obtenerPorCodigo(ParametroEnum.LIMITE_DIARIO_RETIRO.getCodigo());
            BigDecimal saldo = BigDecimal.ZERO;
            if (Objects.isNull(ultimoMovimiento)) {
                ultimoMovimiento = new Movimiento();
            } else {
                saldo = ultimoMovimiento.getSaldo();
            }

            validarMovimiento(listaMovimientoRetiro, movimiento, tipoMovimiento, saldo);

            if (tipoMovimiento.getCodigo().equals(CatalogoEnum.MOVIMIENTO_DEPOSITO.getCodigo())) {
                saldo = saldo.add(movimiento.getValor());
            } else {
                saldo = saldo.subtract(movimiento.getValor());
            }
            ultimoMovimiento = new Movimiento();
            ultimoMovimiento.setCuenta(cuenta);
            ultimoMovimiento.setFecha(new Date());
            ultimoMovimiento.setSaldo(saldo);
            ultimoMovimiento.setTipo(tipoMovimiento);
            ultimoMovimiento.setValor(movimiento.getValor());
            movimientoRepository.save(ultimoMovimiento);
        } catch (RuntimeException e) {
            throw new ApiException(e);
        }
    }

    private void validarMovimiento(List<Movimiento> listaMovimiento, Movimiento movimiento, Catalogo tipoMovimiento, BigDecimal saldo) throws ApiException {
        if (tipoMovimiento.getCodigo().equals(CatalogoEnum.MOVIMIENTO_RETIRO.getCodigo())) {
            BigDecimal valorLimiteRetiro = new BigDecimal(limiteDiario);
            if (Objects.isNull(listaMovimiento) || listaMovimiento.isEmpty()) {
                if (movimiento.getValor().compareTo(valorLimiteRetiro) == 1) {
                    throw new ApiException(Mensajes.CUPO_EXCEDIDO);
                }
                BigDecimal saldos = saldo.subtract(movimiento.getValor());
                if (saldos.compareTo(BigDecimal.ZERO) == -1) {
                    throw new ApiException(Mensajes.SALDO_NO_DISPONIBLE);

                }

            } else {
                validarMovimientoDiario(listaMovimiento, movimiento, valorLimiteRetiro, saldo);
            }
        }
    }

    private void validarMovimientoDiario(List<Movimiento> listaMovimiento, Movimiento movimiento, BigDecimal valorLimiteRetiro, BigDecimal saldo) throws ApiException {
        BigDecimal totalRetiroMovimiento = extraerRetirosDiarios(listaMovimiento, movimiento);
        BigDecimal saldos = saldo.subtract(movimiento.getValor());

        if (totalRetiroMovimiento.compareTo(valorLimiteRetiro) == 1) {
            throw new ApiException(Mensajes.CUPO_EXCEDIDO);
        }
        if (saldos.compareTo(BigDecimal.ZERO) == -1) {
            throw new ApiException(Mensajes.SALDO_NO_DISPONIBLE);
        }
    }

    private BigDecimal extraerRetirosDiarios(List<Movimiento> listaMovimiento, Movimiento movimiento) {
        BigDecimal totalRetiro = BigDecimal.ZERO;
        if (Objects.nonNull(listaMovimiento) && !listaMovimiento.isEmpty()) {
            for (Movimiento mov : listaMovimiento) {
                totalRetiro = totalRetiro.add(mov.getValor());
            }
        }
        totalRetiro = totalRetiro.add(movimiento.getValor());
        return totalRetiro;
    }

    public List<Movimiento> listarTodo() {
        return movimientoRepository.findAll();
    }

    public List<MovimientoTo> listarReporte(String fechaDesde, String fechaHasta, String identificacion) {
        List<MovimientoTo> respuesta = new ArrayList<>();
        Cliente cliente = clienteRepository.obtenerPorIdentificacion(identificacion);
        Cuenta cuenta = cuentaRepository.findByCliente(cliente);
        List<Movimiento> listaMovimientoCliente = movimientoRepository.listarPorFechaMovimientoReporte(cuenta, FechaUtil.obtenerFechaDesde(FechaUtil.convertirCadenaAFecha(FechaUtil.PATRON_FECHA, fechaDesde)),
                FechaUtil.obtenerFechaHasta(FechaUtil.convertirCadenaAFecha(FechaUtil.PATRON_FECHA, fechaHasta)));
        if (Objects.nonNull(listaMovimientoCliente) && !listaMovimientoCliente.isEmpty()) {
            listaMovimientoCliente.stream().map(movimiento -> {
                MovimientoTo mTo = new MovimientoTo();
                mTo.setCliente(cliente.getPersona().getNombre());
                mTo.setEstado(cuenta.getEstado().toString());
                mTo.setFecha(movimiento.getFecha().toString());
                mTo.setTipoCuenta(cuenta.getTipoCuenta().getNombre());
                mTo.setNumeroCuenta(cliente.getPersona().getIdentificacion());
                mTo.setMovimiento(movimiento.getValor().toString());
                mTo.setSaldoDisponible(movimiento.getSaldo().toString());
                return mTo;
            }).forEachOrdered(mTo -> {
                respuesta.add(mTo);
            });
        }
        return respuesta;
    }

}
