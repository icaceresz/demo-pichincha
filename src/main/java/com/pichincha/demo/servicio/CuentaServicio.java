/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.pichincha.demo.servicio;

import com.pichincha.demo.entidad.Catalogo;
import com.pichincha.demo.entidad.Cliente;
import com.pichincha.demo.entidad.Cuenta;
import com.pichincha.demo.entidad.Movimiento;
import com.pichincha.demo.reporitory.CatalogoRepository;
import com.pichincha.demo.reporitory.ClienteRepository;
import com.pichincha.demo.reporitory.CuentaRepository;
import com.pichincha.demo.reporitory.MovimientoRepository;
import com.pichincha.demo.util.ApiException;
import com.pichincha.demo.util.CatalogoEnum;
import com.pichincha.demo.util.Mensajes;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author icaceres
 */
@Service
public class CuentaServicio {

    @Autowired
    private CuentaRepository cuentaRepository;

    @Autowired
    private CatalogoRepository catalogoRepository;

    @Autowired
    private ClienteRepository clienteRepository;

    @Autowired
    private MovimientoRepository movimientoRepository;

    @Transactional
    public void guardar(Cuenta cuenta) throws ApiException {
        try {
            Catalogo tipoCuenta = catalogoRepository.findByCodigo(cuenta.getTipoCuenta().getCodigo());
            Cliente cliente = clienteRepository.obtenerPorIdentificacion(cuenta.getCliente().getPersona().getIdentificacion());
            if (Objects.isNull(cliente)) {
                throw new ApiException("Cliente no existe.");
            } else {
                cuenta.setCliente(cliente);
                cuenta.setTipoCuenta(tipoCuenta);
                Cuenta cuentaGuardada = cuentaRepository.saveAndFlush(cuenta);
                guardarMovimientoInicial(cuentaGuardada, cuenta.getSaldoInicial());
            }
        } catch (RuntimeException e) {
            throw new ApiException(e);
        }
    }

    private void guardarMovimientoInicial(Cuenta cuenta, BigDecimal saldoInicial) throws ApiException {
        try {
            Movimiento movimiento = new Movimiento();
            Catalogo deposito = catalogoRepository.findByCodigo(CatalogoEnum.MOVIMIENTO_DEPOSITO.getCodigo());
            movimiento.setCuenta(cuenta);
            movimiento.setFecha(new Date());
            movimiento.setTipo(deposito);
            movimiento.setSaldo(saldoInicial);
            movimiento.setValor(saldoInicial);
            movimientoRepository.save(movimiento);
        } catch (RuntimeException e) {
            throw new ApiException(e);
        }
    }

    @Transactional
    public void actualizar(Cuenta cuenta, Long id) throws ApiException {
        try {
            Cuenta entidad = cuentaRepository.getById(id);
            Catalogo tipoCuenta = catalogoRepository.findByCodigo(cuenta.getTipoCuenta().getCodigo());
            Cliente cliente = clienteRepository.obtenerPorIdentificacion(cuenta.getCliente().getPersona().getIdentificacion());
            if (Objects.nonNull(entidad)) {
                cuenta.setId(id);
                cuenta.setCliente(cliente);
                cuenta.setTipoCuenta(tipoCuenta);
            }
            cuentaRepository.save(cuenta);
        } catch (RuntimeException e) {
            throw new ApiException(e);
        }
    }

    @Transactional
    public void eliminar(Long id) throws ApiException {
        Cuenta cuenta = cuentaRepository.getById(id);
        if (Objects.isNull(cuenta)) {
            throw new ApiException(Mensajes.REGISTRO_NO_EXISTE);
        } else {
            cuenta.setEstado(Boolean.FALSE);
            cuentaRepository.save(cuenta);
        }
    }

    public List<Cuenta> listarActivos() {
        return cuentaRepository.listarActivos();
    }

}
