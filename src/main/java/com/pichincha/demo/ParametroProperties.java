package com.pichincha.demo;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;

/**
 * @author icaceres
 */
@Configuration
@PropertySources({
    @PropertySource("classpath:parametro.properties")
})
public class ParametroProperties {

}
