/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.pichincha.demo.entidad;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import lombok.Data;

/**
 *
 * @author icaceres
 */
@Entity
@Table(name = "tbl_movimiento")
@Data
public class Movimiento implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "mov_id")
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "cat_tipo_movimiento_id")
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    @NotNull
    private Catalogo tipo;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "cue_id")
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    @NotNull
    private Cuenta cuenta;

    @Column(name = "mov_fecha")
    @Temporal(TemporalType.TIMESTAMP)
    @NotNull
    private Date fecha;

//    @Column(name = "cue_id", insertable = false, updatable = false)
//    private Long idCuenta;
//    
//    @Column(name = "cat_tipo_movimiento_id", insertable = false, updatable = false)
//    private Long idTipoMovimiento;

    @Column(name = "mov_valor")
    @NotNull
    private BigDecimal valor;

    @Column(name = "mov_saldo")
    @NotNull
    private BigDecimal saldo;

    @Transient
    private String identificacionCliente;

}
