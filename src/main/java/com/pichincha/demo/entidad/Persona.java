/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.pichincha.demo.entidad;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import lombok.Data;

/**
 *
 * @author icaceres
 */
@Entity
@Table(name = "tbl_persona")
@Data
public class Persona implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "per_id")
    private Long id;

    @Column(name = "per_nombre", length = 150)
    @NotNull
    private String nombre;

    @Column(name = "per_genero", length = 1)
    @NotNull
    private String genero;
    
    @Column(name = "per_edad")
    @NotNull
    private Integer edad;
    
    @Column(name = "per_identificacion", length = 13, unique = true)
    @NotNull
    private String identificacion;
    
    @Column(name = "per_direccion", length = 500)
    @NotNull
    private String direccion;
    
    @Column(name = "per_telefono", length = 20)
    @NotNull
    private String telefono;
    
}
