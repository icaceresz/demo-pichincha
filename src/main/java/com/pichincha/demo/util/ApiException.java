/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.pichincha.demo.util;

/**
 *
 * @author icaceres
 */
public class ApiException extends Exception {

    public ApiException(String mensaje) {
        super(mensaje);
    }

    public ApiException(Throwable throwable) {
        super(throwable);
    }

    public ApiException(String mensaje, Throwable throwable) {
        super(mensaje, throwable);
    }

}
