/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Enum.java to edit this template
 */
package com.pichincha.demo.util;

import lombok.Getter;

/**
 *
 * @author icaceres
 */
public enum CatalogoEnum {
    
    MOVIMIENTO_DEPOSITO("MD"), MOVIMIENTO_RETIRO("MR");

    @Getter
    private String codigo;

    private CatalogoEnum(String codigo) {
        this.codigo = codigo;
    }
}
