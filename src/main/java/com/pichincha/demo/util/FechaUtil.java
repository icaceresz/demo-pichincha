/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.pichincha.demo.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 *
 * @author icaceres
 */
public class FechaUtil {

    public static final Locale EC_LOCALE = new Locale("es_EC");

    public static final String PATRON_FECHA = "yyyy-MM-dd";

    public static Date obtenerFechaDesde(Date fechaDesde) {
        Calendar fechaDesdeAux = Calendar.getInstance();
        fechaDesdeAux.setTime(fechaDesde);
        fechaDesdeAux.set(Calendar.HOUR_OF_DAY, 0);
        fechaDesdeAux.set(Calendar.MINUTE, 0);
        fechaDesdeAux.set(Calendar.SECOND, 0);
        return fechaDesdeAux.getTime();
    }

    public static Date obtenerFechaHasta(Date fechaHasta) {
        Calendar fechaHastaAux = Calendar.getInstance();
        fechaHastaAux.setTime(fechaHasta);
        fechaHastaAux.set(Calendar.HOUR_OF_DAY, 23);
        fechaHastaAux.set(Calendar.MINUTE, 59);
        fechaHastaAux.set(Calendar.SECOND, 59);
        return fechaHastaAux.getTime();
    }

    public static Date convertirCadenaAFecha(String patron, String fecha) {
        SimpleDateFormat formato = new SimpleDateFormat(patron, EC_LOCALE);
        try {
            return formato.parse(fecha);
        } catch (ParseException ex) {
            return null;
        }
    }

}
