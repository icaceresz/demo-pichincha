/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.pichincha.demo.util;

/**
 *
 * @author icaceres
 */
public class Mensajes {
    
    public static final String REGISTRO_NO_EXISTE= "Registro no existe";
    public static final String CUPO_EXCEDIDO = "Cupo diario Excedido";
    public static final String SALDO_NO_DISPONIBLE = "Saldo no disponible";
    
}
