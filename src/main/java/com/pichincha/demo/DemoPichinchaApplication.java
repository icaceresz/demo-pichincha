package com.pichincha.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoPichinchaApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoPichinchaApplication.class, args);
	}

}
