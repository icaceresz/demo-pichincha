/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.pichincha.demo.rest;

import com.pichincha.demo.entidad.Cliente;
import com.pichincha.demo.servicio.ClienteServicio;
import com.pichincha.demo.util.ApiException;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author icaceres
 */
@Slf4j
@RestController
@RequestMapping("/clientes")
public class ClienteRest {

    @Autowired
    private ClienteServicio clienteServicio;

    @PostMapping("/guardar")
    public ResponseEntity<Cliente> guardar(@RequestBody Cliente cliente) {
        try {
            Cliente clienteGuardado = clienteServicio.guardar(cliente);
            return new ResponseEntity<>(clienteGuardado, HttpStatus.CREATED);
        } catch (ApiException | RuntimeException e) {
            log.error(e.getMessage());
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/actualizar/{id}")
    public ResponseEntity<Cliente> actualizar(@RequestBody Cliente cliente, @PathVariable Long id) {
        try {
            Cliente clienteGuardado = clienteServicio.actualizar(cliente, id);
            return new ResponseEntity<>(clienteGuardado, HttpStatus.CREATED);
        } catch (ApiException | RuntimeException e) {
            log.error(e.getMessage());
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/eliminar/{id}")
    public ResponseEntity<String> eliminar(@PathVariable Long id) {
        try {
            clienteServicio.eliminar(id);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (ApiException | RuntimeException e) {
            log.error(e.getMessage());
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/listarActivos")
    public ResponseEntity<List<Cliente>> listarActivos() {
        List<Cliente> clientes = clienteServicio.listarActivos();
        return new ResponseEntity<>(clientes, HttpStatus.OK);
    }

}
