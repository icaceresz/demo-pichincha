/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.pichincha.demo.rest;

import com.pichincha.demo.entidad.Cuenta;
import com.pichincha.demo.servicio.CuentaServicio;
import com.pichincha.demo.util.ApiException;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author icaceres
 */
@Slf4j
@RestController
@RequestMapping("/cuentas")
public class CuentaRest {

    @Autowired
    private CuentaServicio cuentaServicio;

    @PostMapping("/guardar")
    public ResponseEntity<String> guardar(@RequestBody Cuenta cuenta) {
        try {
            cuentaServicio.guardar(cuenta);
            return new ResponseEntity<>("Cuenta creada", HttpStatus.CREATED);
        } catch (ApiException | RuntimeException e) {
            log.error(e.getMessage());
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/actualizar/{id}")
    public ResponseEntity<String> actualizar(@RequestBody Cuenta cuenta, @PathVariable Long id) {
        try {
            cuentaServicio.actualizar(cuenta, id);
            return new ResponseEntity<>("Cuenta actualizada", HttpStatus.CREATED);
        } catch (ApiException | RuntimeException e) {
            log.error(e.getMessage());
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/eliminar/{id}")
    public ResponseEntity<String> eliminar(@PathVariable Long id) {
        try {
            cuentaServicio.eliminar(id);
            return new ResponseEntity<>("Cuenta eliminada", HttpStatus.OK);
        } catch (ApiException | RuntimeException e) {
            log.error(e.getMessage());
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/listarActivos")
    public ResponseEntity<List<Cuenta>> listarActivos() {
        List<Cuenta> cuentas = cuentaServicio.listarActivos();
        return new ResponseEntity<>(cuentas, HttpStatus.OK);
    }

}
