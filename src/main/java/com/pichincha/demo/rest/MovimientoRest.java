/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.pichincha.demo.rest;

import com.pichincha.demo.entidad.Movimiento;
import com.pichincha.demo.servicio.MovimientoServicio;
import com.pichincha.demo.to.MovimientoTo;
import com.pichincha.demo.util.ApiException;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author icaceres
 */
@Slf4j
@RestController
@RequestMapping("/movimientos")
public class MovimientoRest {
    
    @Autowired
    private MovimientoServicio movimientoServicio;
    
    @PostMapping("/guardar")
    public ResponseEntity<String> guardar(@RequestBody Movimiento movimiento) {
        try {
            movimientoServicio.guardar(movimiento);
            return new ResponseEntity<>("Movimiento registrado", HttpStatus.CREATED);
        } catch (ApiException | RuntimeException e) {
            log.error(e.getMessage());
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    
    @GetMapping("/listarTodos")
    public ResponseEntity<List<Movimiento>> listarTodos() {
        List<Movimiento> movimientos = movimientoServicio.listarTodo();
        return new ResponseEntity<>(movimientos, HttpStatus.OK);
    }
    
    @GetMapping("/listarReporte/{identificacion}/{fechaDesde}/{fechaHasta}")
    public ResponseEntity<List<MovimientoTo>> listarReporte(@PathVariable String identificacion,
            @PathVariable String fechaDesde, @PathVariable String fechaHasta) {
        List<MovimientoTo> movimientos = movimientoServicio.listarReporte(fechaDesde, fechaHasta, identificacion);
        return new ResponseEntity<>(movimientos, HttpStatus.OK);
    }
    
}
