/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.pichincha.demo.to;

import lombok.Data;

/**
 *
 * @author icaceres
 */
@Data
public class MovimientoTo {

    private String fecha;
    private String cliente;
    private String numeroCuenta;
    private String tipoCuenta;
    private String estado;
    private String movimiento;
    private String saldoDisponible;
}
