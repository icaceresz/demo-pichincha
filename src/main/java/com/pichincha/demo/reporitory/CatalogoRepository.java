/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.pichincha.demo.reporitory;

import com.pichincha.demo.entidad.Catalogo;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author icaceres
 */
public interface CatalogoRepository extends JpaRepository<Catalogo, Long> {
    
    Catalogo findByCodigo(String codigo);

}
