/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.pichincha.demo.reporitory;

import com.pichincha.demo.entidad.Cliente;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author icaceres
 */
public interface ClienteRepository extends JpaRepository<Cliente, Long> {
    
    @Query("select c from Cliente c where c.estado= true")
    List<Cliente> listarActivos();
    
    @Query("select c from Cliente c where c.persona.identificacion = :identificacion")
    Cliente obtenerPorIdentificacion(@Param("identificacion") String identificacion);

}
