/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.pichincha.demo.reporitory;

import com.pichincha.demo.entidad.Catalogo;
import com.pichincha.demo.entidad.Cuenta;
import com.pichincha.demo.entidad.Movimiento;
import java.util.Date;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author icaceres
 */
public interface MovimientoRepository extends JpaRepository<Movimiento, Long> {

    @Query(nativeQuery = true, value = "select * from tbl_movimiento m WHERE m.cue_id = :cuentaId order by m.mov_id desc limit 1")
    Movimiento obtenerUltimoPorCuenta(@Param("cuentaId") Long cuentaId);
    
    @Query("select m from Movimiento m WHERE m.cuenta = :cuenta and m.fecha >= :fechaDesde and m.fecha <= :fechaHasta AND m.tipo = :tipo")
    List<Movimiento> listarPorFechaTipoMovimiento(@Param("cuenta") Cuenta cuenta, @Param("fechaDesde") Date fechaDesde, @Param("fechaHasta") Date fechaHasta, @Param("tipo") Catalogo tipo);

    
    @Query("select m from Movimiento m WHERE m.cuenta = :cuenta and m.fecha >= :fechaDesde and m.fecha <= :fechaHasta ORDER BY m.fecha desc")
    List<Movimiento> listarPorFechaMovimientoReporte(@Param("cuenta") Cuenta cuenta, @Param("fechaDesde") Date fechaDesde, @Param("fechaHasta") Date fechaHasta);
}
