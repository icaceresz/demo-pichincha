/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.pichincha.demo.reporitory;

import com.pichincha.demo.entidad.Cliente;
import com.pichincha.demo.entidad.Cuenta;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 *
 * @author icaceres
 */
public interface CuentaRepository extends JpaRepository<Cuenta, Long> {

    @Query("select c from Cuenta c where c.estado= true")
    List<Cuenta> listarActivos();
    
    Cuenta findByCliente(Cliente cliente);
}
