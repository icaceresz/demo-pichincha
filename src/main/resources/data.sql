/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Other/SQLTemplate.sql to edit this template
 */
/**
 * Author:  icaceres
 * Created: 22/03/2022
 */
create table tbl_catalogo (cat_id bigint not null, cat_codigo varchar(20) not null, cat_nombre varchar(50) not null, primary key (cat_id));
create table tbl_cliente (cli_id bigint not null, cli_contrasenia varchar(512) not null, cli_estado boolean not null, per_id bigint not null, primary key (cli_id));
create table tbl_cuenta (cue_id bigint not null, cue_estado boolean not null, cue_saldo_inicial decimal(19,2) not null, cli_id bigint not null, cat_tipo_cuenta_id bigint not null, primary key (cue_id));
create table tbl_movimiento (mov_id bigint not null, mov_fecha timestamp not null, cue_id bigint not null, cat_tipo_movimiento_id bigint not null, mov_saldo decimal(19,2) not null, mov_valor decimal(19,2) not null, primary key (mov_id));
create table tbl_persona (per_id bigint not null, per_direccion varchar(500) not null, per_edad integer not null, per_genero varchar(1) not null, per_identificacion varchar(13) not null, per_nombre varchar(150) not null, per_telefono varchar(20) not null, primary key (per_id));
alter table tbl_cliente drop constraint if exists UK_oom5ubhvm5xk0x7uheyhakglm;
alter table tbl_cliente add constraint UK_oom5ubhvm5xk0x7uheyhakglm unique (per_id);
alter table tbl_cuenta drop constraint if exists UK_c3mljrui3fa35w38j2nhehpaf;
alter table tbl_cuenta add constraint UK_c3mljrui3fa35w38j2nhehpaf unique (cli_id);
alter table tbl_persona drop constraint if exists UK_8rtmjy1dfa6i2hlg69am7qt79;
alter table tbl_persona add constraint UK_8rtmjy1dfa6i2hlg69am7qt79 unique (per_identificacion);
create sequence hibernate_sequence start with 1 increment by 1;
alter table tbl_cliente add constraint FKkls4psuxi4evhdmam2jwaxj2y foreign key (per_id) references tbl_persona;
alter table tbl_cuenta add constraint FK35kt44y2dqqgt7j9mk9t0yt6c foreign key (cli_id) references tbl_cliente;
alter table tbl_cuenta add constraint FKf9rd11rvxrdtcwkfm64gxnv0i foreign key (cat_tipo_cuenta_id) references tbl_catalogo;
alter table tbl_movimiento add constraint FK2ep72jp2r08x6gqlsa7d9uyj8 foreign key (cue_id) references tbl_cuenta;
alter table tbl_movimiento add constraint FKdenql04oi7bqtveestru9t9ju foreign key (cat_tipo_movimiento_id) references tbl_catalogo;

--INICIALIZACION DE DATOS
insert into tbl_catalogo(cat_id, cat_codigo, cat_nombre) values(1,'CA','Ahorros');
insert into tbl_catalogo(cat_id, cat_codigo, cat_nombre) values(2,'CC','Corriente');

insert into tbl_catalogo(cat_id, cat_codigo, cat_nombre) values(3,'MR','Retiro');
insert into tbl_catalogo(cat_id, cat_codigo, cat_nombre) values(4,'MD','Depósito');
